package ua.dp.seven;

public class ImplementConverter implements Converter {
    private String scale;
    private double value;

    public double SearchValue(String string) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            if ((string.charAt(i) != 'K') && (string.charAt(i) != 'F') && (string.charAt(i) != 'C')) {
                sb.append(string.charAt(i));
            }
        }
        this.value = Double.parseDouble(sb.toString());
        this.scale = string.substring(string.length()-1);
        return this.value;
    }
    @Override
    public void Convert(double value) {
        switch (this.scale) {
            case "K":
                System.out.println("F: " + (int)(((value*9)/5)-459.67));
                System.out.println("C: " + (int)(value - 273.15));
                break;
            case "F":
                System.out.println("K: " + (int)((value + 459.67)*5)/9);
                System.out.println("C: " + (int)(((value - 32)*5)/9));
                break;
            case "C":
                System.out.println("F: " + (int)(((value + 32)*9)/5));
                System.out.println("K: " + (int)(value + 273.15));
                break;
            default:
                System.out.println("Данные введены некорректно");
        }
    }
}
