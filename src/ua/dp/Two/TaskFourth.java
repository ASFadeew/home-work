package ua.dp.Two;
import ua.dp.Two.NewArray;
import ua.dp.Two.OutputArray;

public class TaskFourth {
    /*
    Создайте 2 массива из 5 случайных целых чисел из отрезка [0;5] каждый.
    Выведите массивы на экран в двух отдельных строках. Посчитайте среднее
    арифметическое элементов каждого массива и сообщите, для какого из массивов
    это значение оказалось больше (либо сообщите, что их средние арифметические равны)
     */
    public static void main (String[] args){
        int counter;
        double sum = 0;
        double firstArithmeticMean, secondArithmeticMean;
        int [] firstArrayOfRandomNumbers = NewArray.CreateNewArray(0,5, 5);
        int [] secondArrayOfRandomNumbers = NewArray.CreateNewArray(0, 5, 5);
        OutputArray.ArrayOutputOnConcole(firstArrayOfRandomNumbers);
        OutputArray.ArrayOutputOnConcole(secondArrayOfRandomNumbers);
        for (counter = 0; counter<firstArrayOfRandomNumbers.length; counter++) {
            sum += firstArrayOfRandomNumbers[counter];
        }
        firstArithmeticMean = sum/counter;
        sum = 0;
        for (counter = 0; counter<secondArrayOfRandomNumbers.length; counter++) {
            sum += secondArrayOfRandomNumbers[counter];
        }
        secondArithmeticMean = sum/counter;
        if (firstArithmeticMean>secondArithmeticMean){
            System.out.println("Среднее арифметическое первого массива (" + firstArithmeticMean + ") больше чем среднее арифметическое второго массива (" + secondArithmeticMean + ")");
        } else if (firstArithmeticMean<secondArithmeticMean){
            System.out.println("Среднее арифметическое первого массива (" + firstArithmeticMean + ") меньше чем среднее арифметическое второго массива (" + secondArithmeticMean + ")");
        } else {
            System.out.println("Средниe арифметические двух массивов равны ("+firstArithmeticMean+" = "+secondArithmeticMean+")");
        }
    }
}
