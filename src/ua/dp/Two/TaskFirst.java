package ua.dp.Two;
import ua.dp.Two.NewArray;
import ua.dp.Two.OutputArray;

public class TaskFirst {

    /*Создайте массив из 12 случайных целых чисел из отрезка [-15;15].
      Определите какой элемент является в этом массиве максимальным и
      сообщите индекс его последнего вхождения в массив.
     */

    public static void main (String[] args) {
        int maxValue = 0;
        int maxValueIndex = 0;
        int [] arrayOfRandomNumbers = NewArray.CreateNewArray(-15, 15, 12);
        OutputArray.ArrayOutputOnConcole(arrayOfRandomNumbers);
        for (int i = 0; i<arrayOfRandomNumbers.length; i++){
            if (maxValue<=arrayOfRandomNumbers[i]){
                maxValue = arrayOfRandomNumbers[i];
                maxValueIndex = i;
            }
        }
        System.out.println("Индекс последнего вхождения максимального значения в массив - " + maxValueIndex);
    }
}
