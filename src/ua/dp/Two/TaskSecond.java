package ua.dp.Two;
import ua.dp.Two.NewArray;
import ua.dp.Two.OutputArray;

public class TaskSecond {
    /*
      Создайте массив из 8 случайных целых чисел из отрезка [1;10].
      Выведите массив на экран в строку. Замените каждый элемент с
      нечётным индексом на ноль. Снова выведете массив на экран на отдельной строке
     */
    public static void main (String[] args) {
        int [] arrayOfRandomNumbers = NewArray.CreateNewArray(1, 10, 8 );
        OutputArray.ArrayOutputOnConcole(arrayOfRandomNumbers);

        for (int i = 0; i<arrayOfRandomNumbers.length; i++){
           if (arrayOfRandomNumbers[i]%2 != 0){
               arrayOfRandomNumbers[i] = 0;
           }
        }
        OutputArray.ArrayOutputOnConcole(arrayOfRandomNumbers);
    }
}
