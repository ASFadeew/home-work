package ua.dp.Two;
import ua.dp.Two.NewArray;
import ua.dp.Two.OutputArray;
public class TaskFifth {
    /*
    Создать массив из 50 случайных целых чисел из отрезка [0;1000] и вывести его на экран.
    Создать второй массив только из чётных элементов первого массива, если они там есть,
    и вывести его на экран.
     */
    public static void main (String[] args){
        int counter = 0;
        int secondArraySize = 0;
        int [] arrayOfRandomNumbers = NewArray.CreateNewArray(0, 1000, 50);
        OutputArray.ArrayOutputOnConcole(arrayOfRandomNumbers);
        for (int i = 0; i<arrayOfRandomNumbers.length; i++) {
            if (arrayOfRandomNumbers[i]%2 == 0){
                secondArraySize++;
            }
        }
        int [] arrayOfEvenNumbers = new int[secondArraySize];
        for (int i = 0; i<arrayOfRandomNumbers.length; i++){
             if(arrayOfRandomNumbers[i]%2 == 0) {
                 arrayOfEvenNumbers[counter] = arrayOfRandomNumbers[i];
                 counter++;
             }
        }
        OutputArray.ArrayOutputOnConcole(arrayOfEvenNumbers);
    }
}
