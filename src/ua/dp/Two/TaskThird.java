package ua.dp.Two;
import ua.dp.Two.NewArray;
import ua.dp.Two.OutputArray;

public class TaskThird {
    /*
    Создайте массив из 4 случайных целых чисел из отрезка [10;99].
    Выведите его на экран в строку. Далее определите и выведите на
    экран сообщение о том, является ли массив строго возрастающей последовательностью.
     */
    public static void main (String[] args){
        int counter = 0;
        int [] arrayOfRandomNumbers = NewArray.CreateNewArray(10, 99, 4);
        OutputArray.ArrayOutputOnConcole(arrayOfRandomNumbers);

        for (int i = 1; i<arrayOfRandomNumbers.length; i++){
            if (arrayOfRandomNumbers[counter]<arrayOfRandomNumbers[i]){
                counter++;
            }
        }
        if (counter == arrayOfRandomNumbers.length-1){
            System.out.println("Массив является строго возростающей последовательностью");
        } else System.out.println("Массив не является строго возрастающей последовательностью");
    }
}
