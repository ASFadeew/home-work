package ua.dp.five.strings;

public class Arrays {
    public String[] createArrayOfStrings() {
        String[] stringArray = new String[15];
        stringArray[0] = "выполнение задания по строкам";
        stringArray[1] = "найти самую короткую строкy";
        stringArray[2] = "найти самую длинную строкy";
        stringArray[3] = "вывести длину";
        stringArray[4] = "create an array of type string";
        stringArray[5] = "create an array of type int";
        stringArray[6] = "create an array of type long";
        stringArray[7] = "cоздать массив типа double";
        stringArray[8] = "вывести на консоль";
        stringArray[9] = "найти слово радар";
        stringArray[10] = "найти слова-полиндромы";
        stringArray[11] = "написать программу";
        stringArray[12] = "вывод квадратного уравнения";
        stringArray[13] = "первый способ";
        stringArray[14] = "модификатор доступа шалаш";
        return stringArray;
    }
    public String[] arrayOfNames() {
        String[] names = { "Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", "Alex", "Emma", "John", "James", "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate" };
        return names;
    }
    public int[] arrayOfResults() {
        int[] times = { 341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412, 393, 299, 343, 317, 265 };
        return times;
    }
    public int[] createMatrix() {
        int[] matrix = new int[25];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = i + 1;
        }
        return matrix;
    }
}
