package ua.dp.five.strings;

    /*Под каким номером в строке стоит последняя буква “я” в строке "НепонятнаяСтрока".
    Сделать двумя вариантами
     */

public class TaskThird {
    private String testString;

    TaskThird(String str) {
        this.testString = str;
    }

    public void lastIndexSearch() {
        char[] charArrayForThird = testString.toCharArray();
        for (int i = charArrayForThird.length - 1; i >= 0; i--) {
            if(charArrayForThird[i] == 'я') {
                System.out.println("Индекс последней буквы я (первый способ) - " + i);
                break;
            }
        }
        int indexLetter = testString.lastIndexOf('я');
        System.out.println("Индекс последней буквы я (второй способ) - " + indexLetter);
    }
}
