package ua.dp.five.strings;

    /*Написать программу, позволяющую корректно находить
     корни квадратного уравнения.
     */

public class TaskThirteenth {
    private int a;
    private int b;
    private int c;

    TaskThirteenth(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void quadraticEquation() {
        int discrim;
        double x1;
        double x2;
        discrim = (b * b) - (4 * a * c);
        if (discrim > 0) {
            x1 = (-b + Math.sqrt(discrim)) / (2 * a);
            x2 = (-b - Math.sqrt(discrim)) / (2 * a);
            System.out.println("У этого уравнения два корня:" + "\nx1: " + x1 + ", x2: " + x2);
        } else if (discrim == 0) {
            x1 = -b / (2 * a);
            System.out.println("У этого уравнения один корень: \nx1: " + x1);
        } else {
            System.out.println("У этого уравнения нет корней");
        }
    }
}
