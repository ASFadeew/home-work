package ua.dp.five.strings;

    /*Упорядочить и вывести строки в порядке возрастания
     (убывания) значений их длины.
     */

public class TaskFifth {
    private String[] sortedArray;

    TaskFifth(String[] arr) {
        this.sortedArray = arr;
    }

    public void sortStrings() {
        for (int i = 0; i < sortedArray.length; i++) {
            for (int j = i+1; j < sortedArray.length; j++){
                if(sortedArray[i].length() > sortedArray[j].length()) {
                    String string = sortedArray[i];
                    sortedArray[i] = sortedArray[j];
                    sortedArray[j] = string;
                }
            }
        }
        for (int i = 0; i < sortedArray.length; i++) {
            System.out.println(sortedArray[i] + " (" + sortedArray[i].length() + ")");
        }
    }
}
