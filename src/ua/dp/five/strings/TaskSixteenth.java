package ua.dp.five.strings;

    /*Найдите победителя марафона.
            Группа людей участвует в марафоне, их имена и время за которое они пробежали марафон вы можете увидеть ниже.
            Ваша задача найти человека, который быстрее всех пробежал дистанцию и вывести его имя и счет.
            (Опционально) Найдите человека, который прибежал вторым.

            String[] names = { "Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", "Alex", "Emma", "John", "James", "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate" };

            int[] times = { 341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412, 393, 299, 343, 317, 265 };

     */

public class TaskSixteenth {
    private String[] names;
    private int[] times;
    private int firstPlace;
    private int firstPlaceIndex;
    private int secondPlace;
    private int secondPlaceIndex;

    TaskSixteenth(String[] names, int[] times) {
        this.names = names;
        this.times = times;
    }

    public void winnerSearch() {
        for (int i = 1; i < times.length; i++) {
            if (times[i] > times[i-1] && times[i] > firstPlace) {
                firstPlace = times[i];
                firstPlaceIndex = i;
            }
        }
        for (int j = 1; j < times.length; j++) {
            if (times[j] > secondPlace && times[j] < firstPlace) {
                secondPlace = times[j];
                secondPlaceIndex = j;
            }
        }
        System.out.println("Победитель марафона - " + names[firstPlaceIndex] + ". Результат - " + times[firstPlaceIndex]);
        System.out.println("На втором месте - " + names[secondPlaceIndex] + ". Результат - " + times[secondPlaceIndex]);
    }
}
