package ua.dp.five.strings;

    /*Используя оператор switch, написать программу, которая выводит
      на экран сообщения о принадлежности некоторого значения k интервалам
      (-10k, 0], (0, 5], (5, 10], (10, 10k].
     */

public class TaskTenth {

    public void intervalSearch() {
        int i = (int) ((-15) + Math.random() * 15);
        String str = "";
        if (i > (-10000) && i <= 0) {
            str = "first";
        } else if (i > 0 && i <= 5) {
            str = "second";
        } else if (i > 5 && i <= 10) {
            str = "third";
        } else if (i > 10 && i <=10000) {
            str = "fourth";
        }
        switch (str) {
            case "first" :
                System.out.println("Число " + i + " относится к интервалу (-10k, 0]");
                break;
            case "second" :
                System.out.println("Число " + i + " относится к интервалу (0, 5]");
                break;
            case "third" :
                System.out.println("Число " + i + " относится к интервалу (5, 10]");
                break;
            case "fourth" :
                System.out.println("Число " + i + " относится к интервалу (10, 10k]");
                break;
            default:
                System.out.println("Число " + i + " не относится ни к одному из интервалов");
        }
    }
}
