package ua.dp.five.strings;

    /*Найти количество слов, содержащих только символы латинского алфавита,
     а среди них – количество слов с равным числом гласных и согласных букв.
     */

import java.util.*;

public class TaskFifteenth {
    private String[] newArray;

    TaskFifteenth(String[] arr) {
        this.newArray = arr;
    }

    public void countTheWords() {
        String[] arrayWords;
        List<String> latinWords = new ArrayList<>();
        int count1 = 0;
        int count2 = 0;
        int wordCounter = 0;
        int equalCharCount = 0;

        for (int i = 0; i < newArray.length; i++) {
            arrayWords = newArray[i].split("\\s");
            for (int j = 0; j < arrayWords.length; j++) {
                for (int k = 0; k < arrayWords[j].length(); k++) {
                    if (((arrayWords[j].charAt(k) >= 'A') && (arrayWords[j].charAt(k) <= 'Z')) || ((arrayWords[j].charAt(k) >= 'a') && (arrayWords[j].charAt(k) <= 'z'))) {
                        count1++;
                    }
                }
                if (arrayWords[j].length() == count1) {
                    latinWords.add(arrayWords[j]);
                    wordCounter++;
                }
                count1 = 0;
            }
        }
        System.out.println("Количество слов, содержащих только символы латинского алфавита - " + wordCounter);
        for (int i = 0; i < latinWords.size(); i++) {
            for (int j = 0; j < latinWords.get(i).length(); j++) {
                if (latinWords.get(i).charAt(j) == 'a' || latinWords.get(i).charAt(j) == 'o'
                        || latinWords.get(i).charAt(j) == 'i' ||  latinWords.get(i).charAt(j) == 'u'
                        || latinWords.get(i).charAt(j) ==  'y' || latinWords.get(i).charAt(j) ==  'e'
                        || latinWords.get(i).charAt(j) ==  'A' || latinWords.get(i).charAt(j) ==  'O'
                        || latinWords.get(i).charAt(j) ==  'I' || latinWords.get(i).charAt(j) ==  'U'
                        || latinWords.get(i).charAt(j) ==  'Y' || latinWords.get(i).charAt(j) ==  'E') {
                    count2++;
                } else {
                    count2--;
                }
            }
            if (count2== 0) {
                equalCharCount++;
            }
            count2 = 0;
        }
        System.out.println("Количество слов с равным числом гласных и согласных букв - " + equalCharCount);
    }
}
