package ua.dp.five.strings;

    /*Под каким номером в строке стоит первая буква “я” в строке "НепонятнаяСтрока".
     Сделать двумя вариантами
     */

public class TaskSecond {
    private String testString;

    TaskSecond(String str) {
        this.testString = str;
    }
    public void firstIndexSearch() {
        char[] charArrayForSecond = testString.toCharArray();
        for (int i = 0; i < charArrayForSecond.length; i++) {
            if(charArrayForSecond[i] == 'я') {
                System.out.println("Индекс первой буквы я (первый способ) - " + i);
                break;
            }
        }
        int indexLetter = testString.indexOf('я');
        System.out.println("Индекс первой буквы я (второй способ) - " + indexLetter);
    }
}
