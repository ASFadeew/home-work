package ua.dp.five.strings;

    /*Найти слово, состоящее только из различных символов.
     Если таких слов несколько, найти первое из них.
     */

public class TaskEighth {
    private String[] newArray;

    TaskEighth(String[] arr) {
        this.newArray = arr;
    }

    public void wordSearch() {
        String[] arrayWords;
        int counter = 0;
        boolean equalsSymbol = false;
        boolean foundWord = false;
        for (int i = 0; i < newArray.length; i++) {
            arrayWords = newArray[i].split("\\s");
            for (int j = 0; j < arrayWords.length; j++) {
                if (arrayWords[j].length() >= 4) {
                    for (int k = 0; k < arrayWords[j].length() - 1; k++) {
                        for (int l = 0; l < (arrayWords[j].length() - 1) - k; l++) {
                            counter++;
                            if (arrayWords[j].charAt(k) == arrayWords[j].charAt(counter + k)) {
                                equalsSymbol = true;
                                break;
                            } else {
                                equalsSymbol = false;
                            }
                        }
                        counter = 0;
                        if (equalsSymbol == true) {
                            break;
                        }
                    }
                }
                if (equalsSymbol == false) {
                    System.out.println("Первое слово, состоящее из разных символов - " + arrayWords[j]);
                    foundWord = true;
                    equalsSymbol = true;
                }
                if (foundWord == true) {
                    break;
                }
            }
            if (foundWord == true) {
                break;
            }
        }
    }
}
