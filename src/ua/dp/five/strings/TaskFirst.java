package ua.dp.five.strings;

    /*Напишите код, заменяющий в строке все вхождения слова
     «бяка» на «вырезано цензурой»
     */

public class TaskFirst {
    private String testString;

    TaskFirst(String string) {
        this.testString = string;
    }
    public void replaceWord() {
        System.out.println(testString.replaceAll("бяка", "вырезано цензурой"));
    }
}
