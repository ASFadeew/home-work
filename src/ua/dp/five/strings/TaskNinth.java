package ua.dp.five.strings;

    /*Найти все слова-палиндромы. Если таких слов больше одного,
     найти второе из них.
     */

import java.util.ArrayList;
import java.util.List;

public class TaskNinth {
    private String[] newArray;

    TaskNinth(String[] arr) {
        this.newArray = arr;
    }

    public void palindromesSearch() {
        String[] arrayWords;
        List<String> palindromicWords = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < newArray.length; i++) {
            arrayWords = newArray[i].split("\\s");
            for (int j = 0; j < arrayWords.length; j++) {
                if (arrayWords[j].equalsIgnoreCase(new StringBuilder(arrayWords[j]).reverse().toString())) {
                    palindromicWords.add(arrayWords[j]);
                    count++;
                }
            }
        }
        if (count != 0) {
            System.out.println("Второе слово-палиндром - " + palindromicWords.get(1));
        } else {
            System.out.println("Не найдено ни одного слова");
        }
    }
}
