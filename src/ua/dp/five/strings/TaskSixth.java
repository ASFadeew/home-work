package ua.dp.five.strings;

    /*Вывести на консоль те строки, длина которых меньше (больше)
     средней арифметической всех длин строк в массиве, а также длину.
     */

public class TaskSixth {
    private String[] sortedArray;

    TaskSixth(String[] arr) {
        this.sortedArray = arr;
    }

    public void sortByAverageLength() {
        int counter;
        double arithmeticMean = 0;
        for (counter = 0; counter < sortedArray.length; counter++) {
            arithmeticMean += sortedArray[counter].length();
        }
        arithmeticMean /=counter;
        System.out.println("Среднее арифметическое длины строк - " + arithmeticMean);
        for (int i = 0; i < sortedArray.length; i++) {
            if(sortedArray[i].length() <= arithmeticMean) {
                System.out.println("Строка - " + sortedArray[i] + "; длина строки - " + sortedArray[i].length());
            }
        }
    }
}
