package ua.dp.five.strings;

public class Main {
    public static void main(String[] args) {
        TaskFirst taskFirst = new TaskFirst("Это строка со словом бяка");
        //taskFirst.replaceWord();
        TaskSecond taskSecond = new TaskSecond("Непонятная строка");
        //taskSecond.firstIndexSearch();
        TaskThird taskThird = new TaskThird("Непонятная строка");
        //taskThird.lastIndexSearch();
        TaskFourth taskFourth = new TaskFourth(new Arrays().createArrayOfStrings());
        //taskFourth.stringLengthSearch();
        TaskFifth taskFifth = new TaskFifth(new Arrays().createArrayOfStrings());
        //taskFifth.sortStrings();
        TaskSixth taskSixth = new TaskSixth(new Arrays().createArrayOfStrings());
        //taskSixth.sortByAverageLength();
        TaskSeventh taskSeventh = new TaskSeventh(new Arrays().createArrayOfStrings());
        //taskSeventh.wordSearch();
        TaskEighth taskEighth = new TaskEighth(new Arrays().createArrayOfStrings());
        //taskEighth.wordSearch();
        TaskNinth taskNinth = new TaskNinth(new Arrays().createArrayOfStrings());
        //taskNinth.palindromesSearch();
        TaskTenth taskTenth = new TaskTenth();
        //taskTenth.intervalSearch();
        TaskEleventh taskEleventh = new TaskEleventh();
        //taskEleventh.intervalSearch();
        TaskTwelfth taskTwelfth = new TaskTwelfth(new Arrays().createMatrix(), 5, 5);
        //taskTwelfth.showMatrix();
        TaskThirteenth taskThirteenth = new TaskThirteenth(1, 2, -3);
        //taskThirteenth.quadraticEquation();
        TaskFourteenth taskFourteenth = new TaskFourteenth(new Arrays().createArrayOfStrings());
        //taskFourteenth.minDifferentChar();
        TaskFifteenth taskFifteenth = new TaskFifteenth(new Arrays().createArrayOfStrings());
        //taskFifteenth.countTheWords();
        TaskSixteenth taskSixteenth = new TaskSixteenth(new Arrays().arrayOfNames(), new Arrays().arrayOfResults());
        //taskSixteenth.winnerSearch();


    }
}
