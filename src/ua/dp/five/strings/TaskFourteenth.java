package ua.dp.five.strings;

    /*Найти слово, в котором число различных символов минимально.
     Если таких слов несколько, найти первое из них.
     */

public class TaskFourteenth {
    private String[] newArray;

    TaskFourteenth(String[] arr) {
        this.newArray = arr;
    }

    public void minDifferentChar() {
        String[] arrayWords;
        StringBuffer sb = new StringBuffer();
        String string;
        String word = null;
        int minDiffChar = 10;
        int min = 10;
        for (int i = 0; i < newArray.length; i++) {
            arrayWords = newArray[i].split("\\s");
            for (int j = 0; j < arrayWords.length; j++) {
                if (arrayWords[j].length() >= 4) {
                    for (int k = 0; k < arrayWords[j].length(); k++) {
                        string = String.valueOf(arrayWords[j].charAt(k));
                        if (sb.indexOf(string) == -1) {
                            sb.append(string);
                        }
                    }
                    if (min > sb.length() && minDiffChar > sb.length()) {
                        minDiffChar = sb.length();
                        word = arrayWords[j];
                    }
                    sb.delete(0, sb.length());
                }
            }
        }
        System.out.println("Слово, в котором число различных символов минимально - " + word);
    }
}
