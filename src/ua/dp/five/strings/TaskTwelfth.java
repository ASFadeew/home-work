package ua.dp.five.strings;

    /*Написать программу, которая выводит числа от 1 до 25
     в виде матрицы 5x5 слева направо и сверху вниз.
     */

public class TaskTwelfth {
    private int[] matrix;
    int strings;
    int columns;

    TaskTwelfth(int[] mat, int str, int columns) {
        this.matrix = mat;
        this.strings = str;
        this.columns = columns;
    }

    public void showMatrix() {
        int countMatrix = 0;
        for (int i = 0; i < strings; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("[" + matrix[countMatrix] + "] ");
                countMatrix++;
            }
            System.out.println();
        }
    }
}
