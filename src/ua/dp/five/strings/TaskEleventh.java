package ua.dp.five.strings;

    /*Используя оператор switch, написать программу, которая выводит
     на экран сообщения о принадлежности некоторого значения k интервалам
     (-10k, 5], [0, 10], [5, 15], [10, 10k].
     */

public class TaskEleventh {
    public void intervalSearch() {
        int i = (int) ((-15) + Math.random() * 15);
        String str = "";
        if (i == 5) {
            str = "first";
        } else if (i == 10) {
            str = "second";
        } else if (i > (-10000) && i < 5) {
            str = "third";
        } else if (i > 5 && i < 10) {
            str = "fourth";
        } else if (i > 10 && i <= 15) {
            str = "fifth";
        } else if (i > 15) {
            str = "sixth";
        }
        switch (str) {
            case "first" :
                System.out.println("Число " + i + " относится к интервалам (-10k, 5], [0, 10], [5, 15]");
                break;
            case "second" :
                System.out.println("Число " + i + " относится к интервалам [0, 10], [5, 15], [10, 10000]");
                break;
            case "third" :
                System.out.println("Число " + i + " относится к интервалам [-10000, 5], [0, 10]");
                break;
            case "fourth" :
                System.out.println("Число " + i + " относится к интервалам [0, 10], [5, 15]");
                break;
            case "fifth" :
                System.out.println("Число " + i + " относится к интервалам [5, 15], [10, 10000]");
                break;
            case "sixth" :
                System.out.println("Число " + i + " относится к интервалам [10, 10000]");
                break;
            default:
                System.out.println("Число " + i + " не относится ни к одному из интервалов");
        }
    }
}
