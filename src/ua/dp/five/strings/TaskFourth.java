package ua.dp.five.strings;

    /*Найти самую короткую и самую длинную строки.
     Вывести найденные строки и их длину
     */

public class TaskFourth {
    private String[] newArray;

    TaskFourth(String[] arr) {
        this.newArray = arr;
    }

    public void stringLengthSearch() {
        int maxLength = 0;
        int maxLengthIndex = 0;
        int minLengthIndex = 0;
        for(int i = 1; i < newArray.length; i++) {
            if(newArray[i-1].length() < newArray[i].length() && newArray[i].length() > maxLength) {
                maxLength = newArray[i].length();
                maxLengthIndex = i;
            }
        }
        int minLength = maxLength;
        for (int i = 1; i < newArray.length; i++) {
            if(newArray[i-1].length() > newArray[i].length() && newArray[i].length() < minLength) {
                minLength = newArray[i].length();
                minLengthIndex = i;
            }
        }
        System.out.println("Самая длинная строка - " + newArray[maxLengthIndex] + ". Её длина - " + maxLength);
        System.out.println("Самая короткая строка - " + newArray[minLengthIndex] + ". Её длина - " + minLength);
    }
}
