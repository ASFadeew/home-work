package ua.dp.five.strings;

    /*Найти слово, символы в котором идут в строгом порядке возрастания их кодов.
     Если таких слов несколько, найти первое из них.
     */

public class TaskSeventh {
    private String[] newArray;

    TaskSeventh(String[] arr) {
        this.newArray = arr;
    }

    public void wordSearch() {
        String[] arrayWords;
        int counter = 0;
        boolean foundWord = false;
        for (int i = 0; i < newArray.length; i++) {
            arrayWords = newArray[i].split("\\s");
            for (int j = 0; j < arrayWords.length; j++) {
                for (int k = 0; k < arrayWords[j].length()-1; k++) {
                    if(arrayWords[j].codePointAt(k) <= arrayWords[j].codePointAt(k+1)) {
                        counter++;
                    } else {
                        counter = 0;
                        break;
                    }
                }
                if (counter == arrayWords[j].length()-1) {
                    System.out.println("Первое слово, символы в котором идут в строгом порядке возрастания их кодов - " + arrayWords[j]);
                    foundWord = true;
                    break;
                }
            }
            if (foundWord == true) {
                break;
            }
        }
    }
}
