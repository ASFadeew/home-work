package ua.dp.five.fractions;

import java.text.DecimalFormat;

    /*Создайте класс с именем Fraction, содержащий два поля типа int - числитель и знаменатель обыкновенной дроби.
        Конструктор класса должен инициализировать их заданным набором значений.
        - метод класса, который будет выводить дробь в текстовую строку в формате x / y;
        - метод, добавляющий (сложение) к текущей дроби дробь, переданную ему в параметре и возвращающий
          дробь - результат сложения;
        - метод, умножающий (произведение) текущую дробь на число типа double, переданное ему в параметре и
          возвращающий дробь - результат умножения;
        - метод, делящий (деление) текущую дробь на число типа double, переданное ему в параметре и
          возвращающий дробь - результат деления.
    */

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return "Дробь, инициализированна конструктором: " + this.numerator + "/" + this.denominator;
    }
    public String additionFraction(Fraction fraction) {
        int a = (this.numerator * fraction.denominator) + (fraction.numerator * this.denominator);
        int b = this.denominator * fraction.denominator;
        return "Результат сложения двух дробей: " + a + "/" + b;
    }
    public String multiplication(double number) {
        String string = numerator(number - (int) number);
        int currentDenominator = 100;
        int currentNumerator = Integer.parseInt(string);

        currentNumerator = this.numerator * (currentNumerator + (int) number * currentDenominator);
        currentDenominator = this.denominator * currentDenominator;
        while (currentNumerator%2 == 0 && currentDenominator%2 == 0) {
            currentNumerator /=2;
            currentDenominator /=2;
        }
        return "Результат умножения дроби на число типа double: " + currentNumerator + "/" + currentDenominator;
    }
    public String division(double number1) {
        String string1 = numerator(number1 - (int) number1);
        int currentDenominator = 100;
        int currentNumerator = Integer.parseInt(string1);
        int divisionNumerator = currentNumerator;

        currentNumerator = this.numerator * currentDenominator;
        currentDenominator = this.denominator * (divisionNumerator + (int) number1 * currentDenominator);
        while (currentNumerator%2 == 0 && currentDenominator%2 == 0) {
            currentNumerator /=2;
            currentDenominator /=2;
        }
        return "Результат деления дроби на число типа double: " + currentNumerator + "/" + currentDenominator;
    }
    public String numerator(double fractionalPart) {
        DecimalFormat numberFormat = new DecimalFormat("#0.00");
        return numberFormat.format(fractionalPart).substring(2);
    }
}
