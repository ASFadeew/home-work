package ua.dp.five.fractions;

public class Main {
    public static void main(String[] args) {
        Fraction fraction = new Fraction(1,3);

        System.out.println(fraction.toString());
        System.out.println(fraction.additionFraction(new Fraction(3, 5)));
        System.out.println(fraction.multiplication(2.382));
        System.out.println(fraction.division(1.68));
    }
}
