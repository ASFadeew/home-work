package ua.dp.six.abstractClasses;

public class Triangle extends Figure {
    private int a;
    private int b;
    private int c;

    Triangle(int a, int b, int c) {
        figureName = "Треугольник";
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public double perimeterCalculation() {
        int trianglePerimeter;
        trianglePerimeter = this.a + this.b + this.c;
        return trianglePerimeter;
    }
    @Override
    public double squareCalculation() {
        double triangleSquare;
        double halfPerimeter = perimeterCalculation()/2;
        triangleSquare = Math.sqrt(halfPerimeter * ((halfPerimeter - this.a) * (halfPerimeter - this.b) * (halfPerimeter - this.c)));
        return triangleSquare;
    }
}
