package ua.dp.six.abstractClasses;

public class Main {
    public static void main(String[] args) {
        ArrayOfFigures arrayOfFigures = new ArrayOfFigures(new Rectangle(5, 3), new Circle(3), new Triangle(6, 5, 2));
        arrayOfFigures.showFigures();
        Worker worker1 = new HourlyWorker(25.8, "Иван");
        worker1.showSalary();
        Worker worker2 = new FixedWageWorker(5000, "Николай");
        worker2.showSalary();
    }
}
