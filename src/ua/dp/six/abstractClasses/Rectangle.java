package ua.dp.six.abstractClasses;

public class Rectangle extends Figure {
    private int a;
    private int b;

    Rectangle(int a, int b) {
        figureName = "Прямоугольник";
        this.a = a;
        this.b = b;
    }
    @Override
    public double perimeterCalculation() {
        int rectanglePerimeter;
        rectanglePerimeter = 2 * (this.a + this.b);
        return rectanglePerimeter;
    }
    @Override
    public double squareCalculation() {
        int rectangleSquare;
        rectangleSquare = this.a * this.b;
        return rectangleSquare;
    }
}
