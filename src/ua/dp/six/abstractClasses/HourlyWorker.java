package ua.dp.six.abstractClasses;

public class HourlyWorker extends Worker {
    private double ratePerHour;

    HourlyWorker(double ratePerHour, String name) {
        this.ratePerHour = ratePerHour;
        workerName = name;
    }
    @Override
    public double paymentCalculation() {
        double salary;
        salary = 20.8 * 8 * ratePerHour;
        return salary;
    }
}
