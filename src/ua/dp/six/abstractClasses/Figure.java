package ua.dp.six.abstractClasses;

    /*Создать абстрактный класс Figure с методами вычисления площади и периметра,
      а также методом, выводящим информацию о фигуре на экран.
      Создать производные классы: Rectangle (прямоугольник), Circle (круг), Triangle (треугольник)
      со своими методами вычисления площади и периметра.Создать массив n фигур и вывести полную информацию о фигурах на экран.
     */

public abstract class Figure {
    public String figureName;

    public abstract double perimeterCalculation();
    public abstract double squareCalculation();
    public void showFigure() {
        System.out.println(this.figureName);
        System.out.println("Периметр фигуры: " + perimeterCalculation());
        System.out.println("Площадь фигуры: " + squareCalculation());
    }
}
