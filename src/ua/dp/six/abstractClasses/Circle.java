package ua.dp.six.abstractClasses;

public class Circle extends Figure {
    private final double PI = 3.14;
    private int r;

    Circle(int r) {
        figureName = "Круг";
        this.r = r;
    }
    @Override
    public double perimeterCalculation() {
        double circlePerimeter;
        circlePerimeter = 2 * this.PI * this.r;
        return circlePerimeter;
    }
    @Override
    public double squareCalculation() {
        double circleSquare;
        circleSquare = this.PI * this.r * this.r;
        return circleSquare;
    }
}
