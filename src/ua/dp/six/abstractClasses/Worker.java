package ua.dp.six.abstractClasses;

    /*Создать 3 класса(базовый) и 2 предка которые описывают некоторых работников
      с почасовой оплатой (один из предков) и фиксированой оплатой (второй предок).
      Описать в базовом классе абстрактный метод для расчета среднемесячной зарплаты.
      Для «почасовщиков» формула для расчета такая: «среднемесячная зарплата = 20.8*8*ставка в час»,
      для работников с фиксированой оплатой «среднемесячная зарплата = фиксированой месячной оплате».
     */

public abstract class Worker {
    String workerName;

    public abstract double paymentCalculation();
    public void showSalary() {
        System.out.println(this.workerName);
        System.out.println("Средняя зарплата у этого работника: $" + paymentCalculation());
    }
}
