package ua.dp.six.abstractClasses;

public class FixedWageWorker extends Worker {
    private double fixedRate;

    FixedWageWorker(double rate, String name) {
        this.fixedRate = rate;
        workerName = name;
    }
    @Override
    public double paymentCalculation() {
        return this.fixedRate;
    }
}
