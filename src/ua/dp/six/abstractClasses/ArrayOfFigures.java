package ua.dp.six.abstractClasses;

public class ArrayOfFigures {
    Figure[] figures = new Figure[3];

    public ArrayOfFigures(Rectangle rectangle, Circle circle, Triangle triangle) {
        figures[0] = rectangle;
        figures[1] = circle;
        figures[2] = triangle;
    }

    public void showFigures() {
        for (int i = 0; i < this.figures.length; i++) {
            figures[i].showFigure();
        }
    }
}
