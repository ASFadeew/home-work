package ua.dp.six.interfaces;

public class Main {
    public static void main(String[] args) {
        Fraction fraction = new FractionInt(5, 6);
        Fraction fraction1 = new FractionDouble(5.8, 6.6);

        fraction.addition(new FractionInt(4, 5));
        System.out.println(fraction.toString());
        fraction.subtraction(new FractionInt(2, 3));
        System.out.println(fraction.toString());
        fraction.multiplication(5);
        System.out.println(fraction.toString());
        fraction.division(8);
        System.out.println(fraction.toString());

        fraction1.addition(new FractionDouble(4.2, 5.1));
        System.out.println(fraction1.toString());
        fraction1.subtraction(new FractionDouble(2.5, 3.9));
        System.out.println(fraction1.toString());
        fraction1.multiplication(5);
        System.out.println(fraction1.toString());
        fraction1.division(8);
        System.out.println(fraction1.toString());

        ImplementationOfConverter implementation = new ImplementationOfConverter();
        implementation.convert(implementation.searchSum("30 EUR"));
    }
}
