package ua.dp.six.interfaces;

public class FractionInt implements Fraction {
    private int numerator;
    private int denominator;
    private int newNum;
    private int newDenom;

    public FractionInt(int num, int denom) {
        this.numerator = num;
        this.denominator = denom;
    }

    public int getDenominator() {
        return denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    @Override
    public Fraction addition(Fraction fraction) {
        newNum = (this.numerator * ((FractionInt) fraction).getDenominator()) + (((FractionInt) fraction).getNumerator() * this.denominator);
        newDenom = this.denominator * ((FractionInt) fraction).getDenominator();
        return new FractionInt(newNum, newDenom);
    }

    @Override
    public Fraction subtraction(Fraction fraction) {
        newNum = (this.numerator * ((FractionInt) fraction).getDenominator()) - (((FractionInt) fraction).getNumerator() * this.denominator);
        newDenom = this.denominator * ((FractionInt) fraction).getDenominator();
        return new FractionInt(newNum, newDenom);
    }

    @Override
    public Fraction multiplication(Integer integer) {
        this.newNum = this.numerator * integer;
        this.newDenom = this.denominator;
        return new FractionInt(newNum, newDenom);
    }

    @Override
    public Fraction division(Integer integer) {
        this.newNum = this.numerator;
        this.newDenom = this.denominator * integer;
        return new FractionInt(newNum, newDenom);
    }

    @Override
    public String toString() {
        return newNum + "/" + newDenom;
    }
}
