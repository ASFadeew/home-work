package ua.dp.six.interfaces;

public class FractionDouble implements Fraction {
    private double numerator;
    private double denominator;
    private double newNum;
    private double newDenom;

    FractionDouble(double num, double denom) {
        this.numerator = num;
        this.denominator = denom;
    }

    public double getDenominator() {
        return denominator;
    }

    public double getNumerator() {
        return numerator;
    }

    @Override
    public Fraction addition(Fraction fraction) {
        newNum = (this.numerator * ((FractionDouble) fraction).getDenominator()) + (((FractionDouble) fraction).getNumerator() * this.denominator);
        newDenom = this.denominator * ((FractionDouble) fraction).getDenominator();
        return new FractionDouble(newNum, newDenom);
    }

    @Override
    public Fraction subtraction(Fraction fraction) {
        newNum = (this.numerator * ((FractionDouble) fraction).getDenominator()) - (((FractionDouble) fraction).getNumerator() * this.denominator);
        newDenom = this.denominator * ((FractionDouble) fraction).getDenominator();
        return new FractionDouble(newNum, newDenom);
    }

    @Override
    public Fraction multiplication(Integer integer) {
        this.newNum = this.numerator * integer;
        this.newDenom = this.denominator;
        return new FractionDouble(newNum, newDenom);
    }

    @Override
    public Fraction division(Integer integer) {
        this.newNum = this.numerator;
        this.newDenom = this.denominator * integer;
        return new FractionDouble(newNum, newDenom);
    }

    @Override
    public String toString() {
        return newNum + "/" + newDenom;
    }
}
