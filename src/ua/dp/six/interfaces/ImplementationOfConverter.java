package ua.dp.six.interfaces;

public class ImplementationOfConverter implements Converter {
    private double sum;
    private String currency;

    public double searchSum(String string) {
        this.sum = Double.parseDouble(string.substring(0, string.indexOf(' ')));
        this.currency = string.substring(string.lastIndexOf(' '), string.length()).trim();
        return this.sum;
    }
    @Override
    public void convert(double sum) {
        switch (this.currency) {
            case "USD" :
                System.out.println("Конвертация в EUR: " + sum * 0.9);
                System.out.println("Конвертация в UAH: " + sum * 24.3);
                break;
            case "EUR" :
                System.out.println("Конвертация в USD: " + sum * 1.1);
                System.out.println("Конвертация в UAH: " + sum * 26.7);
                break;
            case "UAH" :
                System.out.println("Конвертация в USD: " + sum / 24.3);
                System.out.println("Конвертация в EUR: " + sum / 26.7);
                break;
        }
    }
}
