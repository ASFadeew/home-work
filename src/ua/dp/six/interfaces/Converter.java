package ua.dp.six.interfaces;

    /*Написать конвертацию валюты. Пользователь вводит кол-во денег и наименование валюты.
     Например 15 USD или 20 EU. Использовать интерфейсы. Подсказка: Создать интерфейс Converter с методом convert,
     который принимает цифру для конвертации.
     */

public interface Converter {
    void convert(double sum);
}
