package ua.dp.six.exceptions;

    /*Создайте до 10 исключительных ситуаций и обработайте их.
     (К примеру обработать операцию деления на 0)
     */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Exceptions {
    public int divisionByZero() {
        int a = 10;
        int b = 0;
        int c;
        try {
            c = a / b;
        } catch (ArithmeticException ex) {
            ex.printStackTrace();
            c = 5;
        }
        return c;
    }
    public int stringConversion() {
        String string = "Exception";
        int a = 0;
        try {
            a = Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            a = 10;
        }
        return a;
    }
    public void createFile() {
        File file = new File("Java.txt");
        try {
            Scanner scanner = new Scanner(file);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("File does not exist");
        }
    }
    public void showFirstWord() {
        String string = null;
        String[] arrayWords;
        try {
            arrayWords = string.split(" ");
            System.out.println(arrayWords[0]);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }
}
