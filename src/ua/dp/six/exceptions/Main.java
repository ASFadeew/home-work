package ua.dp.six.exceptions;

public class Main {
    public static void main(String[] args) {
        Time time = new Time(36, 24, 70);
        //System.out.println(time.toString());
        time.setHour(22);
        time.setMinute(26);
        time.setSecond(48);
        //System.out.println(time.toString());

        Exceptions exc1 = new Exceptions();
        System.out.println(exc1.divisionByZero());
        System.out.println(exc1.stringConversion());
        exc1.createFile();
        exc1.showFirstWord();
    }
}
