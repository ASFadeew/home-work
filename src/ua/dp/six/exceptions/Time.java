package ua.dp.six.exceptions;

    /*Составить описание класса для представления времени.
     Предусмотреть возможности установки времени и изменения его отдельных
     полей (час, минута, секунда) с проверкой допустимости вводимых значений.
     В случае недопустимых значений полей выбрасываются исключения.
     Создать методы изменения времени на заданное количество часов, минут и секунд.
     */

public class Time {
    private int hour;
    private int minute;
    private int second;

    Time(int hour, int minute, int second) {
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }

    public void setHour(int hour) {
        try {
            if (hour > 0 && hour < 24) {
                this.hour = hour;
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            this.hour = 23;
        }
    }
    public int getHour() {
        return this.hour;
    }
    public void setMinute(int minute) {
        try {
            if (minute > 0 && minute < 60) {
                this.minute = minute;
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            this.minute = 59;
        }
    }
    public int getMinute() {
        return this.minute;
    }
    public void setSecond(int second) {
        try {
            if (second > 0 && second < 60) {
                this.second = second;
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            this.second = 59;
        }
    }
    public int getSecond() {
        return this.second;
    }
    @Override
    public String toString() {
        return "Время: " + this.hour + ":" + this.minute + ":" + this.second;
    }
}
