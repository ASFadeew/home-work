package ua.dp.four;

public class Student {
    class DOB {
        private int day;
        private int month;
        private int year;
        private DOB (int d, int m, int y) {
            this.day = d;
            this.month = m;
            this.year = y;
        }
    }
    class StudentAddress {
        private String street;
        private int house;
        private int apartment;
        private StudentAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private String faculty;
    private String group;
    private long phone;
    private int yearOfStady;
    private DOB dateOfBirth;
    private StudentAddress address;
    private static int studentId = 0;

    public Student() {
        this.id = studentId;
        studentId++;
    }
    public Student(String surname, String name, String middleName, String faculty, String group, long phone, int yearOfStady, int d, int m, int y, String s, int h, int a) {
        this.id = studentId;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.faculty = faculty;
        this.group = group;
        this.phone = phone;
        this.yearOfStady = yearOfStady;
        dateOfBirth = new DOB(d, m, y);
        address = new StudentAddress(s, h, a);
        studentId++;
    }
    public int getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new StudentAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
    public String getFaculty() {
        return this.faculty;
    }
    public void setGroup(String group) {
        this.group = group;
    }
    public String getGroup() {
        return this.group;
    }
    public void setPhone(long phone) {
        this.phone = phone;
    }
    public long getPhone() {
        return this.phone;
    }
    public void setYearOfStady(int yearOfStady) {
        this.yearOfStady = yearOfStady;
    }
    public int getYearOfStady() {
        return this.yearOfStady;
    }
    public void setDateOfBirth(int d, int m, int y) {
        dateOfBirth = new DOB(d, m, y);
    }
    public String getDateOfBirth() {
        return this.dateOfBirth.day + "." + this.dateOfBirth.month + "." + this.dateOfBirth.year;
    }
    public static Student objectOfStudentClass() {
        Student student = new Student();
        return student;
    }
    public static Student objectOfStudentClass(String surname, String name, String middleName, String faculty, String group, long phone, int yearOfStady, int d, int m, int y, String s, int h, int a) {
        Student student1 = new Student(surname, name, middleName, faculty, group, phone, yearOfStady, d, m, y, s, h, a);
        return student1;
    }
}
