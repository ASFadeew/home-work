package ua.dp.four;

public class Customer {
    class CustomerAddress {
        private String street;
        private int house;
        private int apartment;
        private CustomerAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private long cardNumber;
    private long accountNumber;
    private CustomerAddress address;
    private static int customerId = 0;

    public Customer() {
        this.id = customerId;
        customerId++;
    }
    public Customer(String surname, String name, String middleName, long cardNumber, long accountNumber, String street, int house, int apartment) {
        this.id = customerId;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        address = new CustomerAddress(street, house, apartment);
        customerId++;
    }
    public int getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new CustomerAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }
    public long getCardNumber() {
        return this.cardNumber;
    }
    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }
    public long getAccountNumber() {
        return this.accountNumber;
    }
    public static Customer objectOfCustomerClass() {
        Customer customer = new Customer();
        return customer;
    }
    public static Customer objectOfCustomerClass(String surname, String name, String middleName, long cardNumber, long accountNumber, String street, int house, int apartment) {
        Customer customer1 = new Customer(surname, name, middleName, cardNumber, accountNumber, street, house, apartment);
        return customer1;
    }
}