package ua.dp.four;

public class Book {
    private int id;
    private String title;
    private String author;
    private String publisher;
    private String binding;
    private int year;
    private int pages;
    private double price;
    private static int bookId;

    public Book() {
        this.id = bookId;
        bookId++;
    }
    public Book(String title, String author, String publisher, String binding, int year, int pages, double price) {
        this.id = bookId;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.binding = binding;
        this.year = year;
        this.pages = pages;
        this.price = price;
        bookId++;
    }
    public int getId() {
        return this.id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return this.title;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getAuthor() {
        return this.author;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public String getPublisher() {
        return this.publisher;
    }
    public void setBinding(String binding) {
        this.binding = binding;
    }
    public String getBinding() {
        return this.binding;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return this.year;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }
    public int getPages() {
        return this.pages;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
    public static Book objectOfBookClass() {
        Book book = new Book();
        return book;
    }
    public static Book objectOfBookClass(String title, String author, String publisher, String binding, int year, int pages, double price) {
        Book book1 = new Book(title, author, publisher, binding, year, pages, price);
        return book1;
    }
}
