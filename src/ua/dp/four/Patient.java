package ua.dp.four;

public class Patient {
    class PatientAddress {
        private String street;
        private int house;
        private int apartment;
        private PatientAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private String diagnosis;
    private long phone;
    private int cardNumber;
    private PatientAddress address;
    private static int patientId = 0;

    public Patient() {
        this.id = patientId;
        patientId++;
    }
    public Patient(String surname, String name, String middleName, String diagnosis, long phone, int cardNumber, String street, int house, int apartment) {
        this.id = patientId;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.diagnosis = diagnosis;
        this.phone = phone;
        this.cardNumber = cardNumber;
        address = new PatientAddress(street, house, apartment);
        patientId++;
    }
    public int getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new PatientAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public String getDiagnosis() {
        return this.diagnosis;
    }
    public void setPhone(long phone) {
        this.phone = phone;
    }
    public long getPhone() {
        return this.phone;
    }
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCardNumber() {
        return this.cardNumber;
    }
    public static Patient objectOfPatientClass() {
        Patient patient = new Patient();
        return patient;
    }
    public static Patient objectOfPatientClass(String surname, String name, String middleName, String diagnosis, long phone, int cardNumber, String street, int house, int apartment) {
        Patient patient1 = new Patient(surname, name, middleName, diagnosis, phone, cardNumber, street, house, apartment);
        return patient1;
    }
}
