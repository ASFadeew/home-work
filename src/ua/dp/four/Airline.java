package ua.dp.four;

public class Airline {
    private int id;
    private String destination;
    private String flightNumber;
    private String typeOfAircraft;
    private String departureTime;
    private String [] daysOfTheWeek;
    private static int airlineId;

    public Airline() {
        this.id = airlineId;
        airlineId++;
    }
    public Airline(String destination, String flightNumber, String typeOfAircraft, String departureTime, String [] daysOfTheWeek) {
        this.id = airlineId;
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.typeOfAircraft = typeOfAircraft;
        this.departureTime = departureTime;
        this.daysOfTheWeek = daysOfTheWeek;
        airlineId++;
    }
    public int getId() {
        return this.id;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDestination() {
        return this.destination;
    }
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return this.flightNumber;
    }
    public void setTypeOfAircraft(String typeOfAircraft) {
        this.typeOfAircraft = typeOfAircraft;
    }
    public String getTypeOfAircraft() {
        return this.typeOfAircraft;
    }
    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }
    public String getDepartureTime() {
        return this.departureTime;
    }
    public void setDaysOfTheWeek(String [] daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }
    public String [] getDaysOfTheWeek() {
        return this.daysOfTheWeek;
    }
    public static Airline objectOfAirlineClass() {
        Airline airline = new Airline();
        return airline;
    }
    public static Airline objectOfAirlineClass(String destination, String flightNumber, String typeOfAircraft, String departureTime, String [] daysOfTheWeek) {
        Airline airline1 = new Airline(destination, flightNumber, typeOfAircraft, departureTime, daysOfTheWeek);
        return airline1;
    }
}
