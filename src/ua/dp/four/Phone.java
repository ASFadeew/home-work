package ua.dp.four;

public class Phone {
    class PhoneAddress {
        private String street;
        private int house;
        private int apartment;

        private PhoneAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    class TimeForCity {
        private int cityHours;
        private int cityMinutes;
        private int citySeconds;

        private TimeForCity(int cityHours, int cityMinutes, int citySeconds) {
            this.cityHours = cityHours;
            this.cityMinutes = cityMinutes;
            this.citySeconds = citySeconds;
        }
    }
    class TimeForCountry {
        private int countryHours;
        private int countryMinutes;
        private int countrySeconds;

        private TimeForCountry(int countryHours, int countryMinutes, int countrySeconds) {
            this.countryHours = countryHours;
            this.countryMinutes = countryMinutes;
            this.countrySeconds = countrySeconds;
        }
    }
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private double debit;
    private double kredit;
    private int cardNumber;
    private PhoneAddress address;
    private TimeForCity cityTalks;
    private TimeForCountry countryTalks;
    private static int phoneId;

    public Phone() {
        this.id = phoneId;
        phoneId++;
    }
    public Phone(String surname, String name, String middleName, double debit, double kredit, int cardNumber, String street, int house, int apartment) {
        this.id = phoneId;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.debit = debit;
        this.kredit = kredit;
        this.cardNumber = cardNumber;
        address = new PhoneAddress(street, house, apartment);
        phoneId++;
    }
    public Phone(int cityH, int cityM, int cityS, int countryH, int countryM, int countryS) {
        this.id = phoneId;
        cityTalks = new TimeForCity(cityH, cityM, cityS);
        countryTalks = new TimeForCountry(countryH, countryM, countryS);
        phoneId++;
    }
    public int getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new PhoneAddress(s, h, a);
    }
    public String getAddress() {
        return "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setDebit(double debit) {
        this.debit = debit;
    }
    public double getDebit() {
        return this.debit;
    }
    public void setKredit(double kredit) {
        this.kredit = kredit;
    }
    public double getKredit() {
        return this.kredit;
    }
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCardNumber() {
        return this.cardNumber;
    }
    public void setCityTalks(int cityH, int cityM, int cityS) {
        cityTalks = new TimeForCity(cityH, cityM, cityS);
    }
    public String getCityTalks() {
        return this.cityTalks.cityHours + " часов " + this.cityTalks.cityMinutes + " минут " + this.cityTalks.citySeconds + " секунд";
    }
    public void setCountryTalks(int countryH, int countryM, int countryS) {
        countryTalks = new TimeForCountry(countryH, countryH, countryH);
    }
    public String getCountryTalks() {
        return this.countryTalks.countryHours + " часов " + this.countryTalks.countryMinutes + " минут " + this.countryTalks.countrySeconds + " секунд";
    }
    public static Phone objectOfPhoneClass() {
        Phone phone = new Phone();
        return phone;
    }
    public static Phone objectOfPhoneClass(String surname, String name, String middleName, double debit, double kredit, int cardNumber, String street, int house, int apartment) {
        Phone phone1 = new Phone(surname, name, middleName, debit, kredit, cardNumber, street, house, apartment);
        return phone1;
    }
    public static Phone objectOfPhoneClass(int cityH, int cityM, int cityS, int countryH, int countryM, int countryS) {
        Phone phone2 = new Phone(cityH, cityM, cityS, countryH, countryM, countryS);
        return phone2;
    }
}
