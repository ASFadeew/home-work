package ua.dp.four;

public class Product {
    private int id;
    private String productName;
    private String productUPS;
    private String manufacturer;
    private String shelfLife;
    private double price;
    private double quantity;
    private static int productId;

    public Product() {
        this.id = productId;
        productId++;
    }
    public Product(String productName, String productUPS, String manufacturer, String shelfLife, double price, double quantity) {
        this.id = productId;
        this.productName = productName;
        this.productUPS = productUPS;
        this.manufacturer = manufacturer;
        this.shelfLife = shelfLife;
        this.price = price;
        this.quantity = quantity;
        productId++;
    }
    public int getId() {
        return this.id;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductUPS(String productUPS) {
        this.productUPS = productUPS;
    }
    public String getProductUPS() {
        return this.productUPS;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }
    public String getShelfLife() {
        return this.shelfLife;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
    public double getQuantity() {
        return this.quantity;
    }
    public static Product objectOfProductClass() {
        Product product = new Product();
        return product;
    }
    public static Product objectOfProductClass(String productName, String productUPS, String manufacturer, String shelfLife, double price, double quantity) {
        Product product1 = new Product(productName, productUPS, manufacturer, shelfLife, price, quantity);
        return product1;
    }
}

