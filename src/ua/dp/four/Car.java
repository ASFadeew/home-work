package ua.dp.four;

public class Car {
    private int id;
    private String carMark;
    private String carModel;
    private String color;
    private String registrationNumber;
    private double price;
    private int yearOfManufacture;
    private static int carId;

    public Car() {
        this.id = carId;
        carId++;
    }
    public Car(String carMark, String carModel, String color, String registrationNumber, double price, int yearOfManufacture) {
        this.id = carId;
        this.carMark = carMark;
        this.carModel = carModel;
        this.color = color;
        this.registrationNumber = registrationNumber;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
        carId++;
    }
    public int getId() {
        return this.id;
    }
    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }
    public String getCarMark() {
        return this.carMark;
    }
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }
    public String getCarModel() {
        return this.carModel;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getColor() {
        return this.color;
    }
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
    public String getRegistrationNumber() {
        return this.registrationNumber;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }
    public int getYearOfManufacture() {
        return this.yearOfManufacture;
    }
    public static Car objectOfCarClass() {
        Car car = new Car();
        return car;
    }
    public static Car objectOfCarClass(String carMark, String carModel, String color, String registrationNumber, double price, int yearOfManufacture) {
        Car car1 = new Car(carMark, carModel, color, registrationNumber, price, yearOfManufacture);
        return car1;
    }
}

