package ua.dp.four;

import static ua.dp.four.Abiturient.*;
import static ua.dp.four.Airline.*;
import static ua.dp.four.Book.*;
import static ua.dp.four.Bus.*;
import static ua.dp.four.Car.*;
import static ua.dp.four.Customer.*;
import static ua.dp.four.House.*;
import static ua.dp.four.Patient.*;
import static ua.dp.four.Phone.*;
import static ua.dp.four.Product.*;
import static ua.dp.four.Student.*;
import static ua.dp.four.Train.*;

public class Main {
    public static void main (String [] args) {
        Student std = new Student();
        Customer cmr = new Customer();
        Patient ptn = new Patient();
        Abiturient abt = new Abiturient();
        Book bk = new Book();
        House hs = new House();
        Phone phn = new Phone();
        Car cr = new Car();
        Product prd = new Product();
        Train trn = new Train();
        Bus bs = new Bus();
        Airline arn = new Airline();

        System.out.print("Class Student: " + std.getId() + ", " + objectOfStudentClass().getId());
        System.out.print("\nClass Customer: " + cmr.getId() + ", " + objectOfCustomerClass().getId());
        System.out.print("\nClass Patient: " + ptn.getId() + ", " + objectOfPatientClass().getId());
        System.out.print("\nClass Abiturient: " + abt.getId() + ", " + objectOfAbiturientClass().getId());
        System.out.print("\nClass Book: " + bk.getId() + ", " + objectOfBookClass().getId());
        System.out.print("\nClass House: " + hs.getId() + ", " + objectOfHouseClass().getId());
        System.out.print("\nClass Phone: " + phn.getId() + ", " + objectOfPhoneClass().getId());
        System.out.print("\nClass Car: " + cr.getId() + ", " + objectOfCarClass().getId());
        System.out.print("\nClass Product: " + prd.getId() + ", " + objectOfProductClass().getId());
        System.out.print("\nClass Train: " + trn.getId() + ", " + objectOfTrainClass().getId());
        System.out.print("\nClass Bus: " + bs.getId() + ", " + objectOfBusClass().getId());
        System.out.print("\nClass Airline: " + arn.getId() + ", " + objectOfAirlineClass().getId());
    }
}
