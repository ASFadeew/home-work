package ua.dp.four;

public class Bus {
    private int id;
    private String driverSNM;
    private String routeNumber;
    private String busBrand;
    private int busNumber;
    private int startExploitation;
    private int mileage;
    private static int busId;

    public Bus() {
        this.id = busId;
        busId++;
    }
    public Bus(String driverSNM, String routeNumber, String busBrand, int busNumber, int startExploitation, int mileage) {
        this.id = busId;
        this.driverSNM = driverSNM;
        this.routeNumber = routeNumber;
        this.busBrand = busBrand;
        this.busNumber = busNumber;
        this.startExploitation = startExploitation;
        this.mileage = mileage;
        busId++;
    }
    public int getId() {
        return this.id;
    }
    public void setDriverSNM(String driverSNM) {
        this.driverSNM = driverSNM;
    }
    public String getDriverSNM() {
        return this.driverSNM;
    }
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }
    public String getRouteNumber() {
        return this.routeNumber;
    }
    public void setBusBrand(String busBrand) {
        this.busBrand = busBrand;
    }
    public String getBusBrand() {
        return this.busBrand;
    }
    public void setBusNumber(int busNumber) {
        this.busNumber = busNumber;
    }
    public int getBusNumber() {
        return this.busNumber;
    }
    public void setStartExploitation(int startExploitation) {
        this.startExploitation = startExploitation;
    }
    public int getStartExploitation() {
        return this.startExploitation;
    }
    public void setMileage(int mileage) {
        this.mileage = mileage;
    }
    public int getMileage() {
        return this.mileage;
    }
    public static Bus objectOfBusClass() {
        Bus bus = new Bus();
        return bus;
    }
    public static Bus objectOfBusClass(String driverSNM, String routeNumber, String busBrand, int busNumber, int startExploitation, int mileage) {
        Bus bus1 = new Bus(driverSNM, routeNumber, busBrand, busNumber, startExploitation, mileage);
        return bus1;
    }
}
