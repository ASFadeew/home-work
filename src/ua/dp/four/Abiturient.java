package ua.dp.four;

public class Abiturient {
    class AbiturientAddress {
        private String street;
        private int house;
        private int apartment;
        private AbiturientAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private int id;
    private String surname;
    private String name;
    private String middleName;
    private long phone;
    private int [] assessments;
    private AbiturientAddress address;
    private static int abiturientId = 0;

    public Abiturient() {
        this.id = abiturientId;
        abiturientId++;
    }
    public Abiturient(String surname, String name, String middleName, long phone, int [] assessments, String street, int house, int apartment) {
        this.id = abiturientId;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.phone = phone;
        this.assessments = assessments;
        address = new AbiturientAddress(street, house, apartment);
        abiturientId++;
    }
    public int getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new AbiturientAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setPhone(long phone) {
        this.phone = phone;
    }
    public long getPhone() {
        return this.phone;
    }
    public void setAssessments(int [] assessments) {
        this.assessments = assessments;
    }
    public int [] getAssessments() {
        return this.assessments;
    }
    public static Abiturient objectOfAbiturientClass() {
        Abiturient abiturient = new Abiturient();
        return abiturient;
    }
    public static Abiturient objectOfAbiturientClass(String surname, String name, String middleName, long phone, int [] assessments, String street, int house, int apartment) {
        Abiturient abiturient1 = new Abiturient(surname, name, middleName, phone, assessments, street, house, apartment);
        return abiturient1;
    }
}
