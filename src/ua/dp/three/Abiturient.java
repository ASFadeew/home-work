package ua.dp.three;

public class Abiturient {
    class AbiturientAddress {
        private String street;
        private int house;
        private int apartment;
        private AbiturientAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private String id;
    private String surname;
    private String name;
    private String middleName;
    private int phone;
    private int [] assessments;
    private AbiturientAddress address;

    public Abiturient() {
    }
    public Abiturient(String id, String surname, String name, String middleName, int phone, int [] assessments, String street, int house, int apartment) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.phone = phone;
        this.assessments = assessments;
        address = new AbiturientAddress(street, house, apartment);
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new AbiturientAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }
    public int getPhone() {
        return this.phone;
    }
    public void setAssessments(int [] assessments) {
        this.assessments = assessments;
    }
    public int [] getAssessments() {
        return this.assessments;
    }
}
