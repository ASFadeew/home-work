package ua.dp.three;

public class Phone {
    class PhoneAddress {
        private String street;
        private int house;
        private int apartment;

        private PhoneAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    class TimeForCity {
        private int cityHours;
        private int cityMinutes;
        private int citySeconds;

        private TimeForCity(int cityHours, int cityMinutes, int citySeconds) {
            this.cityHours = cityHours;
            this.cityMinutes = cityMinutes;
            this.citySeconds = citySeconds;
        }
    }
    class TimeForCountry {
        private int countryHours;
        private int countryMinutes;
        private int countrySeconds;

        private TimeForCountry(int countryHours, int countryMinutes, int countrySeconds) {
            this.countryHours = countryHours;
            this.countryMinutes = countryMinutes;
            this.countrySeconds = countrySeconds;
        }
    }
    private String id;
    private String surname;
    private String name;
    private String middleName;
    private double debit;
    private double kredit;
    private int cardNumber;
    private PhoneAddress address;
    private TimeForCity cityTalks;
    private TimeForCountry countryTalks;

    public Phone() {
    }
    public Phone(String id, String surname, String name, String middleName, double debit, double kredit, int cardNumber, String street, int house, int apartment) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.debit = debit;
        this.kredit = kredit;
        this.cardNumber = cardNumber;
        address = new PhoneAddress(street, house, apartment);
    }
    public Phone(int cityH, int cityM, int cityS, int countryH, int countryM, int countryS) {
        cityTalks = new TimeForCity(cityH, cityM, cityS);
        countryTalks = new TimeForCountry(countryH, countryM, countryS);
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new PhoneAddress(s, h, a);
    }
    public String getAddress() {
        return "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setDebit(double debit) {
        this.debit = debit;
    }
    public double getDebit() {
        return this.debit;
    }
    public void setKredit(double kredit) {
        this.kredit = kredit;
    }
    public double getKredit() {
        return this.kredit;
    }
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCardNumber() {
        return this.cardNumber;
    }
    public void setCityTalks(int cityH, int cityM, int cityS) {
        cityTalks = new TimeForCity(cityH, cityM, cityS);
    }
    public String getCityTalks() {
        return this.cityTalks.cityHours + " часов " + this.cityTalks.cityMinutes + " минут " + this.cityTalks.citySeconds + " секунд";
    }
    public void setCountryTalks(int countryH, int countryM, int countryS) {
        countryTalks = new TimeForCountry(countryH, countryH, countryH);
    }
    public String getCountryTalks() {
        return this.countryTalks.countryHours + " часов " + this.countryTalks.countryMinutes + " минут " + this.countryTalks.countrySeconds + " секунд";
    }
}
