package ua.dp.three;

public class House {
    private String id;
    private String street;
    private String typeOfBuilding;
    private int apartmentNumber;
    private double apartmentArea;
    private int floor;
    private int numberOfRooms;
    private int timeExploitation;

    public House() {
    }
    public House(String id, String street, String typeOfBuilding, int apartmentNumber, double apartmentArea, int floor, int numberOfRooms, int timeExploitation) {
        this.id = id;
        this.street = street;
        this.typeOfBuilding = typeOfBuilding;
        this.apartmentNumber = apartmentNumber;
        this.apartmentArea = apartmentArea;
        this.floor = floor;
        this.numberOfRooms = numberOfRooms;
        this.timeExploitation = timeExploitation;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getStreet() {
        return this.street;
    }
    public void setTypeOfBuilding(String typeOfBuilding) {
        this.typeOfBuilding = typeOfBuilding;
    }
    public String getTypeOfBuilding() {
        return this.typeOfBuilding;
    }
    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }
    public int getApartmentNumber() {
        return this.apartmentNumber;
    }
    public void setApartmentArea(double apartmentArea) {
        this.apartmentArea = apartmentArea;
    }
    public double getApartmentArea() {
        return this.apartmentArea;
    }
    public void setFloor(int floor) {
        this.floor = floor;
    }
    public int getFloor() {
        return this.floor;
    }
    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }
    public int getNumberOfRooms() {
        return this.numberOfRooms;
    }
    public void setTimeExploitation(int timeExploitation) {
        this.timeExploitation = timeExploitation;
    }
    public int getTimeExploitation() {
        return this.timeExploitation;
    }
}
