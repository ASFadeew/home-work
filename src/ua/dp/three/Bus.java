package ua.dp.three;

public class Bus {
    private String id;
    private String driverSNM;
    private String routeNumber;
    private String busBrand;
    private int busNumber;
    private int startExploitation;
    private int mileage;

    public Bus() {
    }
    public Bus(String id, String driverSNM, String routeNumber, String busBrand, int busNumber, int startExploitation, int mileage) {
        this.id = id;
        this.driverSNM = driverSNM;
        this.routeNumber = routeNumber;
        this.busBrand = busBrand;
        this.busNumber = busNumber;
        this.startExploitation = startExploitation;
        this.mileage = mileage;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setDriverSNM(String driverSNM) {
        this.driverSNM = driverSNM;
    }
    public String getDriverSNM() {
        return this.driverSNM;
    }
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }
    public String getRouteNumber() {
        return this.routeNumber;
    }
    public void setBusBrand(String busBrand) {
        this.busBrand = busBrand;
    }
    public String getBusBrand() {
        return this.busBrand;
    }
    public void setBusNumber(int busNumber) {
        this.busNumber = busNumber;
    }
    public int getBusNumber() {
        return this.busNumber;
    }
    public void setStartExploitation(int startExploitation) {
        this.startExploitation = startExploitation;
    }
    public int getStartExploitation() {
        return this.startExploitation;
    }
    public void setMileage(int mileage) {
        this.mileage = mileage;
    }
    public int getMileage() {
        return this.mileage;
    }
}
