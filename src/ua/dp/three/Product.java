package ua.dp.three;

public class Product {
    private String id;
    private String productName;
    private String productUPS;
    private String manufacturer;
    private String shelfLife;
    private double price;
    private double quantity;

    public Product() {
    }
    public Product(String id, String productName, String productUPS, String manufacturer, String shelfLife, double price, double quantity) {
        this.id = id;
        this.productName = productName;
        this.productUPS = productUPS;
        this.manufacturer = manufacturer;
        this.shelfLife = shelfLife;
        this.price = price;
        this.quantity = quantity;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductUPS(String productUPS) {
        this.productUPS = productUPS;
    }
    public String getProductUPS() {
        return this.productUPS;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }
    public String getShelfLife() {
        return this.shelfLife;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
    public double getQuantity() {
        return this.quantity;
    }
}
