package ua.dp.three;

public class Train {
    class QuantityOfPlaces {
        private int total;
        private int economyPlace;
        private int compartment;
        private int lux;

        private QuantityOfPlaces(int total, int economyPlace, int compartment, int lux) {
            this.total = total;
            this.economyPlace = economyPlace;
            this.compartment = compartment;
            this.lux = lux;
        }
    }
    private String id;
    private String destination;
    private String trainNumber;
    private String departureTime;
    private QuantityOfPlaces placesInTheTrain;

    public Train() {
    }

    public Train(String id, String destination, String trainNumber, String departureTime, int total, int economyPlace, int compartment, int lux) {
        this.id = id;
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
        placesInTheTrain = new QuantityOfPlaces(total, economyPlace, compartment, lux);
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDestination() {
        return this.destination;
    }
    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }
    public String getTrainNumber() {
        return this.trainNumber;
    }
    public void setDepartureTime(String departureTime) {
        this.destination = departureTime;
    }
    public String getDepartureTime() {
        return this.departureTime;
    }
    public void setPlacesInTheTrain(int total, int economPlaces, int coupePlaces, int luxPlaces) {
        placesInTheTrain = new QuantityOfPlaces(total, economPlaces, coupePlaces, luxPlaces);
    }
    public String getPlacesInTheTrain() {
        return this.placesInTheTrain.total + " - всего мест. Из них  " + this.placesInTheTrain.economyPlace + " плацакртных мест, " + this.placesInTheTrain.compartment + " мест купе, и " + this.placesInTheTrain.lux + " мест класса Люкс";
    }
}
