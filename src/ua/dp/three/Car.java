package ua.dp.three;

public class Car {
    private String id;
    private String carMark;
    private String carModel;
    private String color;
    private String registrationNumber;
    private double price;
    private int yearOfManufacture;

    public Car() {
    }
    public Car(String id, String carMark, String carModel, String color, String registrationNumber, double price, int yearOfManufacture) {
        this.id = id;
        this.carMark = carMark;
        this.carModel = carModel;
        this.color = color;
        this.registrationNumber = registrationNumber;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }
    public String getCarMark() {
        return this.carMark;
    }
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }
    public String getCarModel() {
        return this.carModel;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getColor() {
        return this.color;
    }
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
    public String getRegistrationNumber() {
        return this.registrationNumber;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }
    public int getYearOfManufacture() {
        return this.yearOfManufacture;
    }
}

