package ua.dp.three;

public class Book {
    private String id;
    private String title;
    private String author;
    private String publisher;
    private String binding;
    private int year;
    private int pages;
    private double price;

    public Book() {
    }
    public Book(String id, String title, String author, String publisher, String binding, int year, int pages, double price) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.binding = binding;
        this.year = year;
        this.pages = pages;
        this.price = price;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return this.title;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getAuthor() {
        return this.author;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public String getPublisher() {
        return this.publisher;
    }
    public void setBinding(String binding) {
        this.binding = binding;
    }
    public String getBinding() {
        return this.binding;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return this.year;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }
    public int getPages() {
        return this.pages;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return this.price;
    }
}
