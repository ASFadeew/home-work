package ua.dp.three;

public class Patient {
    class PatientAddress {
        private String street;
        private int house;
        private int apartment;
        private PatientAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private String id;
    private String surname;
    private String name;
    private String middleName;
    private String diagnosis;
    private int phone;
    private int cardNumber;
    private PatientAddress address;

    public Patient() {
    }
    public Patient(String id, String surname, String name, String middleName, String diagnosis, int phone, int cardNumber, String street, int house, int apartment) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.diagnosis = diagnosis;
        this.phone = phone;
        this.cardNumber = cardNumber;
        address = new PatientAddress(street, house, apartment);
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new PatientAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public String getDiagnosis() {
        return this.diagnosis;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }
    public int getPhone() {
        return this.phone;
    }
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCardNumber() {
        return this.cardNumber;
    }
}
