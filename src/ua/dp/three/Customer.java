package ua.dp.three;

public class Customer {
    class CustomerAddress {
        private String street;
        private int house;
        private int apartment;
        private CustomerAddress(String street, int house, int apartment) {
            this.street = street;
            this.house = house;
            this.apartment = apartment;
        }
    }
    private String id;
    private String surname;
    private String name;
    private String middleName;
    private int cardNumber;
    private int accountNumber;
    private CustomerAddress address;

    public Customer() {
    }
    public Customer(String id, String surname, String name, String middleName, int cardNumber, int accountNumber, String street, int house, int apartment) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        address = new CustomerAddress(street, house, apartment);
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return this.surname;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getMiddleName() {
        return this.middleName;
    }
    public void setAddress(String s, int h, int a) {
        address = new CustomerAddress(s, h, a);
    }
    public String getAddress() {
        return  "улица " + this.address.street + ", " + this.address.house + "/" + this.address.apartment;
    }
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }
    public int getCardNumber() {
        return this.cardNumber;
    }
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }
    public int getAccountNumber() {
        return this.accountNumber;
    }
}
