package ua.dp.three;

public class Airline {
    private String id;
    private String destination;
    private String flightNumber;
    private String typeOfAircraft;
    private String departureTime;
    private String [] daysOfTheWeek;

    public Airline() {
    }
    public Airline(String id, String destination, String flightNumber, String typeOfAircraft, String departureTime, String [] daysOfTheWeek) {
        this.id = id;
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.typeOfAircraft = typeOfAircraft;
        this.departureTime = departureTime;
        this.daysOfTheWeek = daysOfTheWeek;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDestination() {
        return this.destination;
    }
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return this.flightNumber;
    }
    public void setTypeOfAircraft(String typeOfAircraft) {
        this.typeOfAircraft = typeOfAircraft;
    }
    public String getTypeOfAircraft() {
        return this.typeOfAircraft;
    }
    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }
    public String getDepartureTime() {
        return this.departureTime;
    }
    public void setDaysOfTheWeek(String [] daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }
    public String [] getDaysOfTheWeek() {
        return this.daysOfTheWeek;
    }
}
